#TEST APP 1

## App requires 2 env vars to be set

- `PORT` - port that the app listents to
- `API_SECRET` - secret, pick any secret you will use then with the app

App is dockerized all errors and logs are >> to STDOUT STDERR

## Available endpoints (all with GET method)

- `/ok` - always return 200
- `/error` - always returns 500
- `/api` - returns 401 if not APP_SECRET provided and returns 200 if APP_SECRET given eg.  `/api?APP_SECRET=mySecret`
- `/api/heavy` - returns 200, does some 'heavy computation'

## Typical user journey is to (might be helpfull for loadtests)

- 3x `/ok`
- then 1x `/error`
- then `/api` without api secret 
- then `/api` with api secret
- and 2x /api/heavy

## Addicional info
- Generate app secret on your own ensure it's stored safely
