# Main Task

To answer the main question: we expect peaks of 400 people within 5 minutes and average of 2000 people during the day (mostly after they finish working).

The answer is: we need at least something like AWS EC2 t2.medium. 

Below short explanation:

400 people = requests (3xok, 1xerror, 1xapi, etc) in 300s, which gives us 1,3 req/s to meet that goal.

On the tests with 4CPU machine we were able to reach 29req/s but that is a bit misleading as it counts every get as request.
```
$ time locust -H http://192.168.1.51:1522 -f locustfile.py -c 8 -r 8 -n 400 --no-web
real	0m12,350s
 Name                                                          # reqs      # fails     Avg     Min     Max  |  Median   req/s
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api                                                           0  59(100.00%)       0       0       0  |       0    0.00
 GET /api/heavy                                                    96     0(0.00%)     439     409     504  |     440    8.67
 GET /api?API_SECRET=alamakota                                     49     0(0.00%)       4       3      33  |       4    4.78
 GET /error                                                         0  42(100.00%)       0       0       0  |       0    0.00
 GET /ok                                                          157     0(0.00%)       4       3       8  |       4   14.33
--------------------------------------------------------------------------------------------------------------------------------------------
 Total                                                            302  101(33.44%)                                      27.78
```
Above 27req/s stats has to be divided by 8, because we consider one request as 8 distinctive actions - {ok: 3, error: 1, api: 1, apis: 1, heavy: 2}. That is wy we have to set new limit for the test which would be 400 * 8 = 3200.

```
$ time locust -H http://192.168.1.51:1522 -f locustfile.py -c 8 -r 8 -n 3200 --no-web
real	1m30,724s

 Name                                                          # reqs      # fails     Avg     Min     Max  |  Median   req/s
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api                                                           0 396(100.00%)       0       0       0  |       0    0.00
 GET /api/heavy                                                   766     0(0.00%)     915     520    1336  |     920    8.60
 GET /api?API_SECRET=alamakota                                    401     0(0.00%)       4       3      12  |       4    6.50
 GET /error                                                         0 395(100.00%)       0       0       0  |       0    0.00
 GET /ok                                                         1249     0(0.00%)       4       2      27  |       4   15.40
--------------------------------------------------------------------------------------------------------------------------------------------
 Total                                                           2416  791(32.74%)                                      30.50
```

The expected peak - 400 people's work can be done in 1m30s, which is below the expected 5min, so we are fine. Let's see what ES2 instance will be appropriate for that.

Testing on t2.micro ec2-52-213-104-126.eu-west-1.compute.amazonaws.com
```
$ time locust -H http://ec2-52-213-104-126.eu-west-1.compute.amazonaws.com:1522 -f locustfile.py -c 8 -r 8 -n 3200 --no-web
real	6m53,297s
 Name                                                          # reqs      # fails     Avg     Min     Max  |  Median   req/s
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api                                                           0 378(100.00%)       0       0       0  |       0    0.00
 GET /api/heavy                                                   828     0(0.00%)    3661    1593    5446  |    3700    2.00
 GET /api?API_SECRET=alamakota                                    406     0(0.00%)     104      72     234  |      96    0.80
 GET /error                                                         0 389(100.00%)       0       0       0  |       0    0.00
 GET /ok                                                         1206     0(0.00%)     102      72     297  |      94    3.70
--------------------------------------------------------------------------------------------------------------------------------------------
 Total                                                           2440  767(31.43%)                                       6.50
```
(100% host CPU, default 2 gunicorn threads 50% busy as the t2.micro has 1xCPU)

-> that is a little bit too slow. We completed the load in 6m53s and need below 5min.

Let's pick something stronger. t2.medium has 2 CPUs
```
$ ssh -i remik-ireland.pem ubuntu@ec2-34-243-36-80.eu-west-1.compute.amazonaws.com
-> add my standard key to authorized_keys
$ ssh ubuntu@ec2-34-243-36-80.eu-west-1.compute.amazonaws.com
$ sudo apt-get install python
```

Back on laptop
```
$ cd ~/scripto/ansible
$ cat aws.ini
[application]
app01 ansible_ssh_host=ec2-34-243-36-80.eu-west-1.compute.amazonaws.com

[all:vars]
ansible_ssh_user=ubuntu

$ ansible -i aws.ini -m ping all
$ ansible-playbook -i aws.ini playbook.yml 
```
And back on EC2
```
$ sudo docker run -d --net=host --name=container_test1 --restart always image_test1
-> add 1522 to inbound open ports
http://ec2-34-243-36-80.eu-west-1.compute.amazonaws.com:1522/ok
```

We can start testing
```
$ time locust -H http://ec2-34-243-36-80.eu-west-1.compute.amazonaws.com:1522 -f locustfile.py -c 8 -r 8 -n 3200 --no-web
real	3m19,356s

 Name                                                          # reqs      # fails     Avg     Min     Max  |  Median   req/s
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api                                                           0 424(100.00%)       0       0       0  |       0    0.00
 GET /api/heavy                                                   745     0(0.00%)    1772     654    2787  |    1700    3.80
 GET /api?API_SECRET=alamakota                                    396     0(0.00%)     102      72     214  |      99    1.10
 GET /error                                                         0 407(100.00%)       0       0       0  |       0    0.00
 GET /ok                                                         1235     0(0.00%)     101      72     293  |      95    5.90
--------------------------------------------------------------------------------------------------------------------------------------------
 Total                                                           2376  831(34.97%)                                      10.80
```
-> so it looks we are fine with that.

# Part 1

Picked Ubuntu Server 16.04 LTS (HVM), SSD Volume Type - ami-f90a4880
```
$ ssh -i remik-ireland.pem ubuntu@ec2-52-213-104-126.eu-west-1.compute.amazonaws.com
$ sudo su -
# cd /opt/
# git clone https://bitbucket.org/u9/devops-test1.git

PUB_FILE_NAME=krzysztof.kokoszka
USER_NAME=krzysztofk

PUB_FILE_NAME=krzysztof.skoracki
USER_NAME=krzysztofs

PUB_FILE_NAME=maciej.zasada
USER_NAME=maciejz

PUB_FILE_NAME=maksim.bachurin
USER_NAME=maksimb
```
Run as root for each user:
```
useradd -m -d /home/$USER_NAME -s /bin/bash $USER_NAME
mkdir /home/$USER_NAME/.ssh
cp /opt/devops-test1/public_keys/$PUB_FILE_NAME /home/$USER_NAME/.ssh/authorized_keys
chown -R $USER_NAME:$USER_NAME /home/$USER_NAME/.ssh
chmod 700 /home/$USER_NAME/.ssh
chmod 600 /home/$USER_NAME/.ssh/authorized_keys
usermod -a -G sudo $USER_NAME
usermod -a -G adm $USER_NAME
```
Now enable sudo without being asked for password
```
# update-alternatives --config editor
-> vim.basic
# visudo -f /etc/sudoers.d/90-cloud-init-users
Add the following lines:
krzysztofk ALL=(ALL) NOPASSWD:ALL
krzysztofs ALL=(ALL) NOPASSWD:ALL
maciejz ALL=(ALL) NOPASSWD:ALL
maksimb ALL=(ALL) NOPASSWD:ALL
remik ALL=(ALL) NOPASSWD:ALL
```
Create additional user that can login just with password
```
# vi /etc/ssh/sshd_config
PasswordAuthentication yes
# /etc/init.d/ssh restart
# useradd -m -d /home/rysiek -s /bin/bash rysiek
# passwd rysiek
```

Now I need to transfer that password inteligently

I have already public keys, it should be possible to use them to encrypt the message. Following https://superuser.com/questions/576506/how-to-use-ssh-rsa-public-key-to-encrypt-a-text
```
$ cd devops-test1/public_keys
```
Convert keys to PEM format
```
$ ssh-keygen -f ~/.ssh/id_rsa.pub -e -m PKCS8 > id_rsa.pem.pub
$ ssh-keygen -f krzysztof.kokoszka -e -m PKCS8 > id_rsa.krzysztof.kokoszka.pem.pub
$ ssh-keygen -f krzysztof.skoracki -e -m PKCS8 > id_rsa.krzysztof.skoracki.pem.pub
$ ssh-keygen -f maciej.zasada -e -m PKCS8 > id_rsa.maciej.zasada.pem.pub
$ ssh-keygen -f maksim.bachurin -e -m PKCS8 > id_rsa.maksim.bachurin.pem.pub
```

Encrypt message
```
$ openssl rsautl -encrypt -pubin -inkey id_rsa.pem.pub -ssl -in password.txt -out password_encrypted.txt 
$ openssl rsautl -encrypt -pubin -inkey id_rsa.krzysztof.kokoszka.pem.pub -ssl -in password.txt -out krzysztof.kokoszka.password_encrypted.txt 
$ openssl rsautl -encrypt -pubin -inkey id_rsa.krzysztof.skoracki.pem.pub -ssl -in password.txt -out krzysztof.skoracki.password_encrypted.txt 
$ openssl rsautl -encrypt -pubin -inkey id_rsa.maciej.zasada.pem.pub -ssl -in password.txt -out maciej.zasada.password_encrypted.txt 
$ openssl rsautl -encrypt -pubin -inkey id_rsa.maksim.bachurin.pem.pub -ssl -in password.txt -out maksim.bachurin.password_encrypted.txt 
```

To decrypt it something similar can be used:
```
$ openssl rsautl -decrypt -inkey ~/.ssh/id_rsa -in password_encrypted.txt -out password.txt
```


Actual installation

Install docker
https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-repository
```
# apt-get install apt-transport-https ca-certificates curl software-properties-common
# curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
# add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
# apt-get update
# apt-get install docker-ce
```

Build docker image
```
# cd /opt/devops-test1/src
```

Based on previous tries we need to add few lines:
```
# vi requirements.txt
gunicorn
# vi Dockerfile
ENV PORT=1522
ENV API_SECRET=alamakota
(it looks like that should be API_SECRET not APP_SECRET)
```

Now the actual image build
```
# docker build --tag image_test1 .
# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED              SIZE
image_test1         latest              00a9ef42d52c        About a minute ago   701MB
python              3.6.3               a8f7167de312        4 months ago         691MB

# docker run -d --net=host --name=container_test1 image_test1
# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
cd99fa325b51        image_test1         "/bin/sh -c 'exec gu…"   8 seconds ago       Up 7 seconds                            container_test1
```

Now some tests. ec2-52-213-104-126.eu-west-1.compute.amazonaws.com

First need to allow inbound traffic on port 1522 with security group.

And it should work:

http://ec2-52-213-104-126.eu-west-1.compute.amazonaws.com:1522/ok
it's ok

http://ec2-52-213-104-126.eu-west-1.compute.amazonaws.com:1522/error
nope

http://ec2-52-213-104-126.eu-west-1.compute.amazonaws.com:1522/api
not ok

http://ec2-52-213-104-126.eu-west-1.compute.amazonaws.com:1522/api?API_SECRET=alamakota
it's ok

http://ec2-52-213-104-126.eu-west-1.compute.amazonaws.com:1522/api?API_SECRET=alamaole
not ok

http://ec2-52-213-104-126.eu-west-1.compute.amazonaws.com:1522/api/heavy
the answer of life the universe and everything is 42

Now some requirements:

1. use docker ensure it works even if the app breaks

Looks for me like: let's make sure docker restarts even if the main app dies.

If I log into docker now
```
# docker exec -it container_test1 bash
# kill 1
# docker ps
-> nothing, and now it should restart on its own
```

I need another flag to run container
```
# docker rm container_test1 
# docker run -d --net=host --name=container_test1 --restart always image_test1
```

Again trying to kill the process
```
# docker exec -it container_test1 bash
# kill 1
# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
a9fc4029be8d        image_test1         "/bin/sh -c 'exec gu…"   44 seconds ago      Up 7 seconds                            container_test1
-> so that worked!
```

2. save logs to a file [all logs in docker are printed to STDOUT/STDERR] accessible for non-root user

```
# docker inspect --format='{{.LogPath}}' container_test1
-> that gives json log file, so it is already there, but accessible only for root user
```

We have our users already in adm group, so they can read syslog. We could direct docker logs to syslog as well.

```
# docker info | grep Logging
Logging Driver: json-file
```

Create new file
```
# vi /etc/docker/daemon.json
{
  "log-driver": "syslog"
}

# systemctl restart docker
# docker info | grep Logging
Logging Driver: syslog
```

OK, now recreate container.
```
# docker stop container_test1 
# docker rm container_test1 
# docker run -d --net=host --name=container_test1 --restart always image_test1

# docker inspect --format='{{.LogPath}}' container_test1
-> nothing
```

And now when I generate some action that results in an error, like:
http://ec2-52-213-104-126.eu-west-1.compute.amazonaws.com:1522/error

```
# tail -f /var/log/syslog
May  7 08:32:11 ip-172-31-28-184 6315e37349d0[30331]: ERROR:root:Houston we got a problem
```

# Part 2

I just loved that part.

Ansible Installation
```
# apt-get install python-pip
# pip install ansible
```

Whenever I want to run the playbook on the host it just needs to have:

Able to login there and sudo without beeing asked for password
```
$ ssh-copy-id remik@192.168.1.49
# visudo
remik ALL=(ALL) NOPASSWD:ALL
```

Python needs to be there
```
# apt-get install python
```

That is all. Now I can run playbook from ansible host. Scripts hosts.ini and playbook.yml attached to mail.
```
$ ansible-playbook -i hosts.ini playbook.yml 
```

Magically everything should be ready and I just need to start the container:
```
# docker run -d --net=host --name=container_test1 --restart always image_test1
```

# Part 3

The major bottleneck of the application is the /api/heavy call, which is very CPU intensive. If I were to make some recommendation that would be the first advise to developers - please check if something there maybe can be optimized, as it is killing the performance. Only after definitive answer that this is good and it is supposed to be like that I would try to size other components for the load. That brings us to CPU utilization. With default setup we have gunicorn configured so that it can use 2 workers. With multi CPU machine this means only 2 threads can work on the call. To fully utilize the CPUs we have (as long as we have more than 2) we need to change it and set it at least equal to the number of CPUs. Anyway it is pretty easy to make the machine 100% CPU busy and that is where we need to stop as it makes little point to look anywhere else.

Details of the tests below:

Nice links:
- https://locust.io/
- https://blog.realkinetic.com/load-testing-with-locust-part-1-174040afdf23
- http://software.danielwatrous.com/load-testing-with-locust-io/

Installation on client machine that will be used to launch the tests.
```
$ sudo apt-get -y install python-setuptools python-dev libevent-dev libzmq3-dev python-pip
$ sudo pip install locustio
$ sudo easy_install pyzmq
```

I use for tests server with 4 actual CPU cores + hyper threading which makes it think it has 8 CPUs and 16G of RAM, but it looks like we are gentle on memory. The application is deployed there using the ansible playbook. For the client I use to launch the locust tests standard laptop is used that has 2 CPU cores and 8G RAM. As long as the client is doing fine I do not mention its performance stats, there is one point where the client running the test is the bottleneck and it is explicitly mentioned there.

Starting with the following test setup:
```
$ cat locustfile.py
from locust import HttpLocust, TaskSet

def ok(l):
    l.client.get("/ok")

def error(l):
    l.client.get("/error")

def api(l):
    l.client.get("/api")

def apis(l):
    l.client.get("/api?API_SECRET=alamakota")

def heavy(l):
    l.client.get("/api/heavy")


class UserBehavior(TaskSet):
    tasks = {ok: 3, error: 1, api: 1, apis: 1, heavy: 2}

class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 0
    max_wait = 0

```

I disable any thinking time, so I can use low number of clients to completely stress out the machine. 

Starting with VirtualBox ubuntu machine that has 2 CPUs assigned.

```
$ time locust -H http://192.168.1.51:1522 -f locustfile.py -c 1 -r 1 -n 5000 --no-web
real	8m8,642s
Name                                                          # reqs      # fails     Avg     Min     Max  |  Median   req/s
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api                                                           0 600(100.00%)       0       0       0  |       0    0.00
 GET /api/heavy                                                  1284     0(0.00%)     361     344     486  |     360    2.70
 GET /api?API_SECRET=alamakota                                    612     0(0.00%)       5       4      13  |       5    1.70
 GET /error                                                         0 629(100.00%)       0       0       0  |       0    0.00
 GET /ok                                                         1875     0(0.00%)       5       3      21  |       5    4.70
--------------------------------------------------------------------------------------------------------------------------------------------
 Total                                                           3771 1229(32.59%)                                       9.10

Percentage of the requests completed within given times
 Name                                                           # reqs    50%    66%    75%    80%    90%    95%    98%    99%   100%
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api/heavy                                                   1284    360    360    370    370    370    370    380    390    486
 GET /api?API_SECRET=alamakota                                     612      5      5      6      6      6      7      7      8     13
 GET /ok                                                          1875      5      5      6      6      6      7      8      9     21
------------------------------------------------------------------------------------------------------------------------------
```

-> Just one user doing all the work. 
-> Host with just one gunicorn thread 100% CPU busy, overall CPU 50% busy
-> Now it is pretty clear that we are CPU bound and we should look at why 'GET /api/heavy' is taking so much time, as it is the actual cause of performance issues. Following tests assume we just have to live with it.

Trying with 2 users.

```
$ time locust -H http://192.168.1.51:1522 -f locustfile.py -c 2 -r 2 -n 5000 --no-web
real	4m12,478s

 Name                                                          # reqs      # fails     Avg     Min     Max  |  Median   req/s
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api                                                           0 629(100.00%)       0       0       0  |       0    0.00
 GET /api/heavy                                                  1243     0(0.00%)     384     353     525  |     380    5.00
 GET /api?API_SECRET=alamakota                                    624     0(0.00%)       5       3      43  |       5    1.70
 GET /error                                                         0 621(100.00%)       0       0       0  |       0    0.00
 GET /ok                                                         1884     0(0.00%)       5       3      22  |       5    7.80
--------------------------------------------------------------------------------------------------------------------------------------------
 Total                                                           3751 1250(33.32%)                                      14.50

Percentage of the requests completed within given times
 Name                                                           # reqs    50%    66%    75%    80%    90%    95%    98%    99%   100%
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api/heavy                                                   1243    380    390    390    390    400    400    420    500    525
 GET /api?API_SECRET=alamakota                                     624      5      5      6      6      7      8     10     15     43
 GET /ok                                                          1884      5      5      6      6      7      8     10     12     22
--------------------------------------------------------------------------------------------------------------------------------------------

```
-> Host with 2 gunicorn threads 100% CPU busy, overall CPU 100% busy
-> single execution times are like in the first run, so we just make good use of second CPU

Without changing anything let's just add more clients.

```
$ time locust -H http://192.168.1.51:1522 -f locustfile.py -c 4 -r 4 -n 5000 --no-web
real	5m15,028s

 Name                                                          # reqs      # fails     Avg     Min     Max  |  Median   req/s
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api                                                           0 660(100.00%)       0       0       0  |       0    0.00
 GET /api/heavy                                                  1204     0(0.00%)     678     424    1194  |     700    4.20
 GET /api?API_SECRET=alamakota                                    658     0(0.00%)      83      33     481  |      73    1.50
 GET /error                                                         0 628(100.00%)       0       0       0  |       0    0.00
 GET /ok                                                         1853     0(0.00%)      80      33     523  |      70    6.90
--------------------------------------------------------------------------------------------------------------------------------------------
 Total                                                           3715 1288(34.67%)                                      12.60

Percentage of the requests completed within given times
 Name                                                           # reqs    50%    66%    75%    80%    90%    95%    98%    99%   100%
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api/heavy                                                   1204    700    760    780    800    840    880    920    960   1194
 GET /api?API_SECRET=alamakota                                     658     73     90    100    110    130    150    190    210    481
 GET /ok                                                          1853     70     87     97    100    130    160    190    210    523
---------------------------------------------

```
-> It just made things worse. Worse overall execution time, worse response time.

Ok, let's add another CPU to VM. Before it had 2xCPU, now it will have 3xCPU.

```
$ time locust -H http://192.168.1.51:1522 -f locustfile.py -c 4 -r 4 -n 5000 --no-web
real	4m28,335s

 Name                                                          # reqs      # fails     Avg     Min     Max  |  Median   req/s
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api                                                           0 637(100.00%)       0       0       0  |       0    0.00
 GET /api/heavy                                                  1276     0(0.00%)     798     593     986  |     800    4.80
 GET /api?API_SECRET=alamakota                                    626     0(0.00%)      11       2      45  |      10    1.80
 GET /error                                                         0 599(100.00%)       0       0       0  |       0    0.00
 GET /ok                                                         1865     0(0.00%)      11       2      42  |      10    5.30
--------------------------------------------------------------------------------------------------------------------------------------------
 Total                                                           3767 1236(32.81%)                                      11.90

Percentage of the requests completed within given times
 Name                                                           # reqs    50%    66%    75%    80%    90%    95%    98%    99%   100%
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api/heavy                                                   1276    800    820    840    840    870    890    930    950    986
 GET /api?API_SECRET=alamakota                                     626     10     13     15     16     20     22     26     29     45
 GET /ok                                                          1865     10     12     15     16     20     23     27     30     42
----------------------------------------------------------------------------------
```

-> And now I was expecting 30% better performance, but there is none. We have results similar to 2 clients.
-> Host is using 2x gunicorn processes with 100% thread utilization, overall 60% CPU used
-> Looks like the bottleneck is 2 gunicorn workers that have to manage everything.

No point with adding additional CPUs to the host as long as we can only use 2 of them. 

```
$ ps -ef | grep guni
root      1980  1955  0 14:02 ?        00:00:00 /usr/local/bin/python /usr/local/bin/gunicorn --threads 16 --workers 2 -b :1522 main:app
root      2014  1980 69 14:02 ?        00:05:27 /usr/local/bin/python /usr/local/bin/gunicorn --threads 16 --workers 2 -b :1522 main:app
root      2015  1980 69 14:02 ?        00:05:25 /usr/local/bin/python /usr/local/bin/gunicorn --threads 16 --workers 2 -b :1522 main:app

-> looks like 1 coordinator and 2 workers.
-> No point to test more, I need to change the docker image
```

```
# vi Dockerfile
CMD exec gunicorn --threads 16 --workers 8 -b :$PORT main:app
# docker build --tag image_test2 .
# docker run -d --net=host --name=container_test2 --restart always image_test2
# ps -ef | grep uni
postfix   2318  2316  0 14:02 ?        00:00:00 pickup -l -t unix -u -c
postfix   2319  2316  0 14:02 ?        00:00:00 qmgr -l -t unix -u
root      2698  2681  2 14:17 ?        00:00:00 /usr/local/bin/python /usr/local/bin/gunicorn --threads 16 --workers 8 -b :1522 main:app
root      2722  2698  0 14:17 ?        00:00:00 /usr/local/bin/python /usr/local/bin/gunicorn --threads 16 --workers 8 -b :1522 main:app
root      2724  2698  0 14:17 ?        00:00:00 /usr/local/bin/python /usr/local/bin/gunicorn --threads 16 --workers 8 -b :1522 main:app
root      2725  2698  0 14:17 ?        00:00:00 /usr/local/bin/python /usr/local/bin/gunicorn --threads 16 --workers 8 -b :1522 main:app
root      2728  2698  0 14:17 ?        00:00:00 /usr/local/bin/python /usr/local/bin/gunicorn --threads 16 --workers 8 -b :1522 main:app
root      2729  2698  0 14:17 ?        00:00:00 /usr/local/bin/python /usr/local/bin/gunicorn --threads 16 --workers 8 -b :1522 main:app
root      2730  2698  0 14:17 ?        00:00:00 /usr/local/bin/python /usr/local/bin/gunicorn --threads 16 --workers 8 -b :1522 main:app
root      2732  2698  0 14:17 ?        00:00:00 /usr/local/bin/python /usr/local/bin/gunicorn --threads 16 --workers 8 -b :1522 main:app
root      2736  2698  0 14:17 ?        00:00:00 /usr/local/bin/python /usr/local/bin/gunicorn --threads 16 --workers 8 -b :1522 main:app

$ time locust -H http://192.168.1.51:1522 -f locustfile.py -c 4 -r 4 -n 5000 --no-web
real	2m50,531s

 Name                                                          # reqs      # fails     Avg     Min     Max  |  Median   req/s
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api                                                           0 635(100.00%)       0       0       0  |       0    0.00
 GET /api/heavy                                                  1223     0(0.00%)     537     363     839  |     530    7.30
 GET /api?API_SECRET=alamakota                                    606     0(0.00%)       3       3       8  |       4    3.60
 GET /error                                                         0 638(100.00%)       0       0       0  |       0    0.00
 GET /ok                                                         1901     0(0.00%)       3       2      33  |       4   11.60
--------------------------------------------------------------------------------------------------------------------------------------------
 Total                                                           3730 1273(34.13%)                                      22.50

Percentage of the requests completed within given times
 Name                                                           # reqs    50%    66%    75%    80%    90%    95%    98%    99%   100%
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api/heavy                                                   1223    530    560    580    600    660    700    750    770    839
 GET /api?API_SECRET=alamakota                                     606      4      4      4      4      5      5      6      6      8
 GET /ok                                                          1901      4      4      4      4      5      5      6      6     33
--------------------------------------------------------------------------------------------------------------------------------------------
                 
```

-> Nice, testing with 4 clients and 4 unicorns active, overall 100% CPU used
-> that is the increase I was expecting.
-> no point to continue now, as host CPU is 100% util, we need more CPU

Adding another CPU to VM host, previously 3, now 4 CPU
```
$ time locust -H http://192.168.1.51:1522 -f locustfile.py -c 8 -r 8 -n 5000 --no-web
real	2m25,990s

 Name                                                          # reqs      # fails     Avg     Min     Max  |  Median   req/s
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api                                                           0 621(100.00%)       0       0       0  |       0    0.00
 GET /api/heavy                                                  1259     0(0.00%)     903     522    1295  |     910    8.60
 GET /api?API_SECRET=alamakota                                    611     0(0.00%)       4       2      32  |       4    5.50
 GET /error                                                         0 630(100.00%)       0       0       0  |       0    0.00
 GET /ok                                                         1886     0(0.00%)       4       2      30  |       4   15.10
--------------------------------------------------------------------------------------------------------------------------------------------
 Total                                                           3756 1251(33.31%)                                      29.20

Percentage of the requests completed within given times
 Name                                                           # reqs    50%    66%    75%    80%    90%    95%    98%    99%   100%
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api/heavy                                                   1259    910    960    990   1000   1100   1100   1200   1200   1295
 GET /api?API_SECRET=alamakota                                     611      4      4      5      5      5      6      7     11     32
 GET /ok                                                          1886      4      4      4      5      5      5      7     11     30
----------------------------------------------------------------
```

-> ok, even faster and already CPU on host contantly 100%

Adding another CPU to VM host, previously 4, now 8 CPU.
```
$ time locust -H http://192.168.1.51:1522 -f locustfile.py -c 8 -r 8 -n 5000 --no-web
real	2m36,267s

 Name                                                          # reqs      # fails     Avg     Min     Max  |  Median   req/s
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api                                                           0 600(100.00%)       0       0       0  |       0    0.00
 GET /api/heavy                                                  1228     0(0.00%)     987     599    1689  |     980    7.80
 GET /api?API_SECRET=alamakota                                    649     0(0.00%)       6       3      43  |       5    3.30
 GET /error                                                         0 600(100.00%)       0       0       0  |       0    0.00
 GET /ok                                                         1930     0(0.00%)       5       3      37  |       5   11.10
--------------------------------------------------------------------------------------------------------------------------------------------
 Total                                                           3807 1200(31.52%)                                      22.20

Percentage of the requests completed within given times
 Name                                                           # reqs    50%    66%    75%    80%    90%    95%    98%    99%   100%
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api/heavy                                                   1228    980   1000   1000   1000   1100   1200   1400   1500   1689
 GET /api?API_SECRET=alamakota                                     649      5      5      6      7     10     13     18     24     43
 GET /ok                                                          1930      5      5      6      7      9     13     17     20     37
```

-> So the results are the same, although it seems it is better now. This is the result of the hyperthreading, where we actually have 4 CPU but the machine thinks we have 8. 
-> no increase in performance, but host CPU stats show that we have now reached 100%

OK, let's see how it would look like without the heavy call
```
$ diff locustfile.py locustfile_light.py
20c20
<     tasks = {ok: 3, error: 1, api: 1, apis: 1, heavy: 2}
---
>     tasks = {ok: 3, error: 1, api: 1, apis: 1}

$ time locust -H http://192.168.1.51:1522 -f locustfile_light.py -c 8 -r 8 -n 50000 --no-web
real	1m11,298s

(less stats now as it is just an exercise)

-> It looks like the bottleneck is now the machine I am testing from:

Need more parallel threads for locus testing
$ locust -f locustfile_light.py --slave&
$ locust -f locustfile_light.py --slave&
$ locust -f locustfile_light.py --slave&
$ locust -f locustfile_light.py --slave&
$ locust -f locustfile_light.py --slave&

$ time locust -H http://192.168.1.51:1522 -f locustfile_light.py -c 8 -r 8 -n 10000 --no-web --master

 Name                                                          # reqs      # fails     Avg     Min     Max  |  Median   req/s
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api                                                           0 6662(100.00%)       0       0       0  |       0    0.00
 GET /api?API_SECRET=alamakota                                   6606     0(0.00%)       5       3      47  |       5  198.80
 GET /error                                                         0 6615(100.00%)       0       0       0  |       0    0.00
 GET /ok                                                        20121     0(0.00%)       5       2      45  |       5  603.50
--------------------------------------------------------------------------------------------------------------------------------------------
 Total                                                          26727 13277(49.68%)                                     802.30

Percentage of the requests completed within given times
 Name                                                           # reqs    50%    66%    75%    80%    90%    95%    98%    99%   100%
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api?API_SECRET=alamakota                                    6606      5      5      6      6      6      6      8     11     47
 GET /ok                                                         20121      5      5      6      6      6      6      8     11     45
--------------------------------------------------------------------------------------------------------------------------------------------
```

-> WOW, so now the bottleneck is still the client, and I can make 802req/s with 12% load on the server comparing to previous 30req/s with 100% load on server. That is how it would look like if we could optimize the heavy call.
-> unable to use more than 12% on server
-> bottleneck in my client, despite using multiple locus

Now, just to test how many "real users with thinkink time" does it take to cause the similar load.

That is the best score so far:
```
$ time locust -H http://192.168.1.51:1522 -f locustfile.py -c 8 -r 8 -n 5000 --no-web
real	2m23,091s

 Name                                                          # reqs      # fails     Avg     Min     Max  |  Median   req/s
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api                                                           0 626(100.00%)       0       0       0  |       0    0.00
 GET /api/heavy                                                  1281     0(0.00%)     862     441    1228  |     880    9.10
 GET /api?API_SECRET=alamakota                                    632     0(0.00%)       4       2      15  |       4    4.30
 GET /error                                                         0 623(100.00%)       0       0       0  |       0    0.00
 GET /ok                                                         1845     0(0.00%)       4       2      36  |       4   14.80
--------------------------------------------------------------------------------------------------------------------------------------------
 Total                                                           3758 1249(33.24%)                                      28.20

Percentage of the requests completed within given times
 Name                                                           # reqs    50%    66%    75%    80%    90%    95%    98%    99%   100%
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api/heavy                                                   1281    880    920    950    970   1000   1100   1100   1100   1228
 GET /api?API_SECRET=alamakota                                     632      4      4      5      5      5      6      7      9     15
 GET /ok                                                          1845      4      4      5      5      5      6      8     11     36
------------------------------------------------------------------------------------------------------------------------

$ diff locustfile.py locustfile_think.py
24,25c24,25
<     min_wait = 0
<     max_wait = 0
---
>     min_wait = 1000
>     max_wait = 3000


$ time locust -H http://192.168.1.51:1522 -f locustfile_think.py -c 100 -r 10 -n 5000 --no-web
real	2m46,651s


 Name                                                          # reqs      # fails     Avg     Min     Max  |  Median   req/s
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api                                                           0 651(100.00%)       0       0       0  |       0    0.00
 GET /api/heavy                                                  1322    19(1.42%)    3855     523    7965  |    3800    8.00
 GET /api?API_SECRET=alamakota                                    579    11(1.86%)      51       4     462  |      34    3.20
 GET /error                                                         0 646(100.00%)       0       0       0  |       0    0.00
 GET /ok                                                         1845    26(1.39%)      51       3     514  |      32   11.30
--------------------------------------------------------------------------------------------------------------------------------------------
 Total                                                           3746 1353(36.12%)                                      22.50

Percentage of the requests completed within given times
 Name                                                           # reqs    50%    66%    75%    80%    90%    95%    98%    99%   100%
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api/heavy                                                   1322   3800   4400   4700   4900   5400   5900   6700   7200   7965
 GET /api?API_SECRET=alamakota                                     579     34     52     67     82    110    160    220    260    462
 GET /ok                                                          1845     32     53     71     83    120    160    200    240    514
--------------------------------------------------------------------------------------------------------------------------------------------
```

-> so I again maxed out the VM, 100% CPU, medium load on the client.
-> I am able to create comparative load with 100 users with think time as with 8 users with no think time. Still the think time is pretty low.
